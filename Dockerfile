FROM node:18 as builder
ARG ENVIRONMENT
# Create app in specific folder
WORKDIR /app
COPY . /app

RUN npm install -g bower gulp
# Installed packages & build
RUN yarn install && gulp build:dev --env=development && rm -rf node_modules
# Serve it from Nginx
FROM nginxinc/nginx-unprivileged:1.17.2-alpine
COPY docker-assets/default.conf /etc/nginx/conf.d/default.conf
COPY docker-assets/compression.conf /etc/nginx/conf.d/compression.conf
#
### Copy compiled app output to Nginx
COPY --from=builder /app/build/ /usr/share/nginx/html/jobs
USER 101
