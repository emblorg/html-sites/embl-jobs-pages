import { VfIntro } from "component-templates/VFIntro";
import { VFLoaderBox } from "component-templates/VFLoader/VFLoader";
import { VFShortcutsList } from "component-templates/VFShortcutsList";
import { formatISODate, parseISODateFromTimeHtml } from "helpers/date-helpers";
import { useFetch } from "helpers/useFetch";
import { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { extendJobsObjects } from "services/useFetchJobsList";
import VFHero from "../../component-templates/VFHero";
import { useScrollToTop } from "../../helpers/useScrollToTop";
import { EBICards } from "../EBICards";
import { EmblBreadcrumbs } from "../EmblBreadcrumbs";
import { EmblCards } from "../EmblCards";
import { EmblFooter } from "../EmblFooter";
import { EmblHeader } from "../EmblHeader";
import { Html } from "../Html";
/* Hero section data */
import HeroData from "./data.json";
import "./JobDetails.scss";
import Testimony from "./Testimony";

const shortcutLinks = [
  {
    linkText: "Your role",
    id: "role",
  },
  {
    linkText: "You have",
    id: "youhave",
  },
  {
    linkText: "You might also have",
    id: "alsohave",
  },
  {
    linkText: "Why join us",
    id: "why",
  },
  {
    linkText: "What else you need to know",
    id: "needtoknow",
  },
  {
    linkText: "More about EMBL-EBI",
    id: "about",
  },
];
/*
 *job data format*

  {
  "title": "Project Leader Software – Big data management",
    "field_jobs_advertise_grade": "7 (monthly salary starting at £3,408 after tax)",
    "field_jobs_apply_link": "https://emea3.recruitmentplatform.com/apply-app/pages/application-form?jobId=QH…",
    "field_jobs_categorisation": "Technology",
    "field_jobs_contract_duration": "3 years (renewable)",
    "field_jobs_duty_station": "<p>EMBL-EBI Hinxton</p>\n",
    "field_jobs_group": "Proteins &amp; Protein Families",
    "field_jobs_description": "We are seeking to recruit a Project Leader in Software in charge of Big data management and development in the Protein Function development team at the European Bioinformatics Institute (EBI) located on the Wellcome Trust Genome Campus near Cambridge in the UK.<br /><br />The post-holder must have over 8 years of experience in Java development within a biological database production environment. S(he) will lead the development of big data and distributed technologies in the production of the world leading resource UniProt. He will also lead the computational annotation pipeline of this resource at EMBL-EBI.\n",
    "field_jobs_publish_date": "<time datetime=\"2020-02-04T12:00:00Z\" className=\"datetime\">Tue, 02/04/2020 - 12:00</time>\n",
    "field_jobs_expiration": "<time datetime=\"2020-03-17T12:00:00Z\" className=\"datetime\">Tue, 03/17/2020 - 12:00</time>\n",
    "field_jobs_reference_number": "EBI01603",
    "field_jobs_staff_category": "Staff Member",
    "field_jobs_type": "Software Development and Engineering",
    "field_jobs_what_else_to_know": "To view a copy of the full job description please click <a href=\"https://oc.ebi.ac.uk/s/QK3dDPqGUVbRv9i\">here</a><br /><br />To apply please submit a covering letter and CV through our online system.<br /><br />Applications are welcome from all nationalities and this will continue after Brexit. For more information please see our website. Visa information will be discussed in more depth with applicants selected for interview.<br /><br />3 year staff member contract renewable up to 9 years of service<br /><br />EMBL-EBI is committed to achieving gender balance and strongly encourages applications from women, who are currently under-represented at all levels.<br /><br />Appointment will be based on merit alone.<br /><br />Applications will close at 23:00 GMT on the date listed above.\n",
    "field_jobs_why_join": "At EMBL-EBI, we help scientists realise the potential of ‘big data’ in biology by enabling them to exploit complex information to make discoveries that benefit mankind. <br /><br />Working for EMBL-EBI gives you an opportunity to apply your skills and energy for the greater good. <br /><br />As part of the European Molecular Biology Laboratory (EMBL), we are a non-profit, intergovernmental organisation funded by 27 member states and two associate member states. <br /><br />We are located on the Wellcome Genome Campus near Cambridge in the UK, and our 850 staff are engineers, technicians, scientists and other professionals from all over the world.<br /><br />EMBL is an inclusive, equal opportunity employer offering attractive conditions and benefits appropriate to an international research organisation. The remuneration package comprises a competitive salary, a comprehensive pension scheme and health insurance, educational and other family related benefits where applicable, as well as financial support for relocation and installation. For more information about pay and benefits <a href=\"https://oc.ebi.ac.uk/s/9cORQcWQzflgFN8#pdfviewer\">click here</a><br /><br />We have an informal culture, international working environment and excellent professional development opportunities but one of the really amazing things about us is the concentration of technical and scientific expertise – something you probably won’t find anywhere else.<br /><br />If you’ve ever visited the campus you’ll have experienced first-hand our friendly, collegial and supportive atmosphere, set in the beautiful Cambridgeshire countryside. Our staff also enjoy excellent sports facilities including a gym, a free shuttle bus, an on-site nursery, cafés and restaurant and a library.",
    "field_jobs_you_also_have": "The post holder should be able to work independently and interact well within a team of software developers and biologists. The position requires the ability to work under time pressure and the dedication to communicate frequently with local as well as consortium partners in the project. The successful candidate will have excellent communication skills and interpersonal skills, (s)he should enjoy working in a stimulating and international culture.\n",
    "field_jobs_you_have": "The ideal candidate must have more than 8 years of experience Java programming in a Unix/Linux environment. You will have strong experience with distributed programming and proven experience of Big data processing technologies. A strong knowledge about the design principles of relational (Oracle, PostgreSQL) and non-relational databases (MongoDB) and the indexing technology Apache Solr is also required. You will be highly familiar with standard development tools and with good coding practices and approaches (VCS, OOP, IoC, automated testing, clean coding principles, code reviews). Additionally a strong background in Python programming, Bash scripting, familiarity with with an HPC scheduler like LSF and an understanding of networking are required. You should have years of experience working with protein sequence databases and a good knowledge of UniProt data and data model.\n",
    "field_jobs_your_role": "The primary responsibilities include:\n<ul><li>Lead, design and development of production software to generate large databases by using big data processing technologies like Apache Spark</li>\n<li>Work with the team in the testing of new technologies and their integration into software pipelines</li>\n<li>Development of computational annotation pipelines for a large number of proteins in the UniProt database</li>\n<li>Maintenance and optimization of existing software production pipelines</li>\n<li>Lead and development of Quality Assurance methodologies and Java code standards for the team</li>\n<li>Design, development and maintenance of new modules, enhancements and scripts for the database release cycle</li>\n<li>Coordination between all UniProt Consortium members involved in the database release cycle.</li>\n</ul>\n"
  }

*/

export function JobDetails({ location }) {
  /*
   * Note that its possible that job object may have been provided ("location?.state?.job"), but not necessary as state from react-router
   * If such object present ("providedJob"), use it else fetch object based on refId provided as part of URL
   */
  const providedJob = location?.state?.job;
  const { jobRef } = useParams();
  const { data, loading, error } = useFetch(
    providedJob
      ? null
      : `jobs?field_jobs_reference_number=${jobRef}&_format=json&source=contenthub`
  );
  const [currentJob, setCurrentJob] = useState(providedJob);

  // Scroll to top on job load
  useScrollToTop();

  const isLocationHinxton =
    currentJob?.$stripped_field_jobs_duty_station === "EMBL-EBI Hinxton";
  const isJobStaffCategoryNOTEICATAdvertOnly =
    currentJob?.field_jobs_staff_category !== "EICAT Advertising Only";

  useEffect(() => {
    if (data) {
      const extendedJob = extendJobsObjects(data)[0];
      setCurrentJob(extendedJob);
    }
  }, [data]);

  if (loading) {
    return <VFLoaderBox />;
  }

  if (error) {
    return (
      <div className="vf-box vf-u-background-color--red--light">
        <h3 className="vf-box__heading">Error occurred</h3>
        <p className="vf-box__text">Error details: {error}.</p>
      </div>
    );
  }

  if (!currentJob) {
    return <JobNotFound />;
  }

  return (
    <div>
      {/* {JSON.stringify(currentJob)} */}

      {creteSocialTags(currentJob)}
      <EmblHeader />
      <EmblBreadcrumbs />

      {/* hero section */}
      {creteHeroSection(currentJob)}

      <section
        className="vf-intro"
        id="intro"
        data-vf-google-analytics-region="intro"
      >
        <div></div>
        <div className="vf-stack vf-stack--400">
          <h1
            className="vf-intro__heading"
            dangerouslySetInnerHTML={{ __html: currentJob?.title }}
          ></h1>
          {isLocationHinxton ? (
            <p className="vf-intro__subheading">
              <span>{currentJob.field_jobs_type}</span>
              <span className="vf-intro__text">
                <br />
                Hinxton, UK
              </span>
            </p>
          ) : (
            <p className="vf-intro__subheading">
              <span itemProp="employmentType">
                {currentJob.field_jobs_type}
                <span className="vf-intro__text">
                  <br />
                  {currentJob.$stripped_field_jobs_duty_station}
                </span>
              </span>
            </p>
          )}
        </div>
      </section>
      {isLocationHinxton && (
        <section
          className="embl-grid"
          data-vf-google-analytics-region="link-list-nav"
        >
          <div>
            <h3 className="vf-links__heading">In this job</h3>
          </div>
          <VFShortcutsList links={shortcutLinks} />
        </section>
      )}
      <section className="embl-grid embl-grid--has-centered-content">
        <div>{/* empty */}</div>
        <div className="vf-content">
          {/* About this position */}
          {currentJob.field_jobs_description?.length > 0 && (
            <Html html={currentJob.field_jobs_description} />
          )}
          {currentJob.field_jobs_your_role.length && (
            <>
              <h3 id="role">Your role</h3>
              <Html html={currentJob.field_jobs_your_role} />
            </>
          )}
        </div>
        <section
          className="vf-content"
          data-vf-google-analytics-region="job-advert-intro"
        >
          {/* Apply button */}
          {isJobStaffCategoryNOTEICATAdvertOnly && (
            <a
              className="vf-button vf-button--primary"
              href={currentJob.field_jobs_apply_link}
            >
              Apply now
            </a>
          )}

          {/* More info */}
          <p className="vf-list__item" itemProp="validThrough">
            <strong>Closing date:</strong>{" "}
            {formatISODate(
              parseISODateFromTimeHtml(currentJob.field_jobs_expiration)
            )}
          </p>
          <div className="vf-links vf-links--tight vf-links__list--s">
            <ul className="vf-links__list vf-links__list--secondary | vf-list">
              {currentJob.field_jobs_contract_duration?.length && (
                <li className="vf-list__item" itemProp="employmentType">
                  <p className="vf-links__meta">
                    Contract duration: {currentJob.field_jobs_contract_duration}
                  </p>
                </li>
              )}
              {currentJob.field_jobs_advertise_grade?.length && (
                <li className="vf-list__item" itemProp="baseSalary">
                  <p className="vf-links__meta">
                    Grading: {currentJob.field_jobs_advertise_grade}
                  </p>
                </li>
              )}
              <li className="vf-list__item" itemProp="baseSalary">
                <p className="vf-links__meta">
                  Reference number: {currentJob.field_jobs_reference_number}
                </p>
              </li>
            </ul>
          </div>

          {/* related links */}

          <div className="vf-links vf-links--tight vf-links__list--s vf-links__list--very-easy">
            <h3 className="vf-links__heading">Related</h3>
            <ul className="vf-links__list vf-links__list--secondary | vf-list">
              <li className="vf-list__item">
                <Link className="vf-list__link" to="/">
                  View all EMBL jobs
                </Link>
              </li>
              <li className="vf-list__item">
                <Link className="vf-list__link" to="/alerts">
                  Sign up for job alerts
                </Link>
              </li>
              <li className="vf-list__item">
                <Link className="vf-list__link" to="/partners">
                  View openings from partners
                </Link>
              </li>
            </ul>
          </div>
        </section>
      </section>
      {/* You Have */}
      <section
        className="embl-grid embl-grid--has-centered-content vf-u-margin__bottom--0"
        data-vf-google-analytics-region="description-you-have"
      >
        <div>{/* empty */}</div>
        <div className="vf-content">
          {/* {% markdown %} */}
          {currentJob.field_jobs_you_have && (
            <>
              <h3 id="youhave">You have</h3>
              <Html html={currentJob.field_jobs_you_have} />
            </>
          )}
          {/* {% endmarkdown %} */}
        </div>
      </section>
      {/* You might also have */}
      <section
        className="embl-grid embl-grid--has-centered-content"
        data-vf-google-analytics-region="description-might-also-have"
      >
        <div>{/* empty */}</div>
        <div className="vf-content">
          {currentJob.field_jobs_you_also_have && (
            <>
              <h3 id="alsohave">You might also have</h3>
              <Html html={currentJob.field_jobs_you_also_have} />
            </>
          )}
        </div>
      </section>
      {/* Why join us and video on the right aside */}
      <section
        className="embl-grid embl-grid--has-sidebar"
        data-vf-google-analytics-region="description-why-join-us"
        itemProp="description"
      >
        <div>{/* empty */}</div>
        <div className="vf-content">
          {currentJob.field_jobs_why_join && (
            <>
              <h3 id="why">Why join us</h3>
              <Html html={currentJob.field_jobs_why_join} />
            </>
          )}
        </div>
        {isLocationHinxton && (
          <article className="vf-video-teaser__item | vf-stack vf-stack--400 | vf-content">
            <h2>
              &nbsp;
              {/* offset */}
            </h2>
            {/* style were here */}
            {renderWhyJoinUsVideo(currentJob.field_jobs_type)}
          </article>
        )}
      </section>
      {/* testimony */}
      {isLocationHinxton && <Testimony job={currentJob} />}

      {/* what else you need to know */}
      <section
        className="embl-grid embl-grid--has-centered-content"
        data-vf-google-analytics-region="description-what-else"
        itemProp="description"
      >
        <div>{/* empty */}</div>
        <div className="vf-content">
          {currentJob.field_jobs_what_else_to_know && (
            <>
              <h3 id="needtoknow">What else you need to know</h3>
              <Html html={currentJob.field_jobs_what_else_to_know} />
            </>
          )}
        </div>
      </section>

      {/* bottom buttons */}
      <section
        className="embl-grid embl-grid--has-centered-content"
        data-vf-google-analytics-region="apply-section"
      >
        <div className="vf-section-header">{/* empty */}</div>
        <div className="vf-content">
          {isJobStaffCategoryNOTEICATAdvertOnly && (
            <>
              <a
                href={currentJob.field_jobs_apply_link}
                className="vf-button vf-button--primary"
              >
                Apply now
              </a>
              <p>
                For questions or issues with the application process, please
                contact{" "}
                {isLocationHinxton ? (
                  <a href="mailto:recruitment@ebi.ac.uk">
                    recruitment@ebi.ac.uk
                  </a>
                ) : (
                  <a href="mailto:recruitment@embl.org">recruitment@embl.org</a>
                )}
              </p>
            </>
          )}
          <p>
            <Link
              to="/alerts"
              data-vf-analytics-label="alerts-link-bottom-button"
              className="vf-button vf-button--secondary vf-button--sm"
            >
              Sign up for job alerts
            </Link>
          </p>

          <a
            href={`mailto:?subject=EMBL Position: ${currentJob.title}&body=I thought that the position of ${currentJob.title} - ${currentJob.field_jobs_reference_number} at EMBL might be of interest to you. %0D%0A %0D%0AYou can find out more about this vacancy at: https://www.embl.org/jobs#/position/${currentJob.field_jobs_reference_number}`}
            data-vf-analytics-label="alerts-link-bottom-button"
            className="vf-link"
          >
            Share this job by email
          </a>
        </div>
      </section>
      {isLocationHinxton ? (
        //ebi specific cards
        <EBICards />
      ) : (
        // general embl cards
        <EmblCards />
      )}
      <EmblFooter />
    </div>
  );
}

const whyJoinUsVideos = [
  {
    jobType: "Software Development and Engineering",
    videoLink: "https://www.youtube.com/embed/gXDLvQgF8Hw",
    title: "Developers at EMBL-EBI",
    subtitle: "What it is like to be a developer at EMBL-EBI?",
  },
  {
    jobType: "Postdoctoral Fellowships",
    videoLink: "https://www.youtube.com/embed/V2D46d8V1V4",
    title: "Working at EMBL-EBI",
    subtitle:
      "Join our diverse team and help power the next generation of discoveries in the life sciences.",
  },
];
const whyJoinUsDefaultVideo = {
  jobType: "default",
  videoLink: "https://www.youtube.com/embed/cABhbUHsKIk",
  title: "Working at EMBL-EBI",
  subtitle:
    "Join our diverse team and help power the next generation of discoveries in the life sciences.",
};

function renderWhyJoinUsVideo(jobType) {
  const foundVideo =
    whyJoinUsVideos.find((video) => video.jobType.includes(jobType)) ||
    whyJoinUsDefaultVideo;
  return (
    <>
      <div className="embed-container">
        <iframe
          src={foundVideo.videoLink}
          frameBorder="0"
          allowFullScreen
        ></iframe>
      </div>
      <h4 className="vf-video-teaser__link vf-link">
        <a href={foundVideo.videoLink}>{foundVideo.title}</a>
      </h4>
      <p className="vf-video-teaser__link">{foundVideo.subtitle}</p>
    </>
  );
}

export function JobNotFound() {
  return (
    <>
      <EmblHeader />
      <EmblBreadcrumbs />
      <VfIntro
        vf_intro_heading="Job not found"
        vf_intro_subheading=""
        vf_intro_lede="This job may have been expired."
      />

      <section className="embl-grid">
        <div></div>
        <div className="vf-content">
          <a
            className="vf-button vf-button--primary"
            href="https://www.embl.org/jobs"
          >
            See all jobs
          </a>
        </div>
      </section>
      <EmblFooter />
    </>
  );
}

function creteHeroSection(job) {
  if (!job) {
    return null;
  }

  const heroDataForThisLocation =
    HeroData.hero_section.find(
      (datum) => datum.location === job.$stripped_field_jobs_duty_station
    ) || {};

  return (
    <VFHero
      image_url={heroDataForThisLocation?.image_url}
      heading={heroDataForThisLocation?.heading || "Job at EMBL"}
      subheading={heroDataForThisLocation?.subheading || ""}
    />
  );
}

/* Create social tags for social sharing on Twitter, FB, LinkedIn */
function creteSocialTags(job) {
  if (!job) {
    return null;
  }

  const title = job.title + " - EMBL Job";
  const description = job.$stripped_field_jobs_description || "Jobs at EMBL";
  const url = window.location.href;
  let imageUrl;

  const heroDataForThisLocation = HeroData.hero_section.find(
    (datum) => datum.location === job.$stripped_field_jobs_duty_station
  );
  if (heroDataForThisLocation) {
    imageUrl = heroDataForThisLocation.image_url;
  }

  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={description} />

      {/* OpenGraph tags */}
      <meta name="og:url" content={url} />
      <meta name="og:title" content={title} />
      <meta name="og:description" content={description} />
      {imageUrl && <meta name="og:image" content={imageUrl} />}
      <meta name="og:type" content="website" />

      {/* Twitter Card tags */}
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      {imageUrl && <meta name="twitter:image" content={imageUrl} />}
      <meta name="twitter:card" content="summary" />
    </Helmet>
  );
}
