import { Home } from "./Home";
import { Helmet } from "react-helmet";

export function EBIHome() {
  return (
    <>
      <Helmet>
        {/* Supports iframe resizing on ebi.ac.uk/careers/jobs */}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/4.3.1/iframeResizer.contentWindow.min.js"></script>
      </Helmet>
      <Home
        showLocations={false}
        jobLocation="EMBL-EBI Hinxton"
        isIframe="true"
      />
    </>
  );
}
