import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";

const getInitialFilters = () => ({
  selectedLocations: [],
  selectedJobTypes: [],
  selectedClosingDateSortDir: "DESC",
  searchTerm: "",
  nPostingTargetID: "",
});

export function useUrlFilters() {
  const [searchParams, setSearchParams] = useSearchParams();

  /* Init lazy loaded filters from url */
  const [filters, setFilters] = useState(() => {
    const {
      selectedLocations,
      selectedJobTypes,
      selectedClosingDateSortDir,
      searchTerm,
      nPostingTargetID,
    } = getInitialFilters();

    return {
      selectedLocations: searchParams.getAll("l") || selectedLocations,
      selectedJobTypes: searchParams.getAll("t") || selectedJobTypes,
      selectedClosingDateSortDir:
        searchParams.get("sort") || selectedClosingDateSortDir,
      searchTerm: searchParams.get("s") || searchTerm,
      nPostingTargetID:
        searchParams.get("nPostingTargetID") || nPostingTargetID,
    };
  });

  const resetFilters = () => {
    setFilters(getInitialFilters());
  };

  useEffect(() => {
    const newSearchParams = {
      l: filters.selectedLocations || [],
      t: filters.selectedJobTypes || [],
      ...(filters.searchTerm && { s: filters.searchTerm }),
      ...(filters.selectedClosingDateSortDir === "ASC" && {
        sort: filters.selectedClosingDateSortDir,
      }),
    };

    if (filters.nPostingTargetID) {
      newSearchParams["nPostingTargetID"] = filters.nPostingTargetID;
    }

    setSearchParams(newSearchParams);
  }, [filters]);
  return { filters, setFilters, resetFilters };
}
