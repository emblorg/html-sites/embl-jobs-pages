import { Helmet } from "react-helmet";

export function ContentMeta({ keywords, description, canonical }) {
  return (
    <Helmet>
      <meta name="keywords" content={keywords} />
      <meta name="description" content={description} />
      <link rel="canonical" href={canonical} />
    </Helmet>
  );
}
