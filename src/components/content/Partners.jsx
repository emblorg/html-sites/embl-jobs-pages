import { ContentMeta } from "./components/ContentMeta";
import { EMBLContentTemplate } from "./components/EMBLContentTemplate";

export function Partners() {
  return (
    <EMBLContentTemplate>
      <ContentMeta
        keywords="jobs, open positions, jobs in research, job opportunities"
        description="Openings from EMBL's partners and similar institutions"
        canonical="https://www.embl.org/jobs/partners/"
      />

      {/* Breadcrumbs */}
      <nav
        className="vf-breadcrumbs vf-u-margin__bottom--600"
        aria-label="Breadcrumb"
      >
        <ul className="vf-breadcrumbs__list | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/"
              className="vf-breadcrumbs__link"
            >
              Jobs
            </a>
          </li>
          <li className="vf-breadcrumbs__item" aria-current="location">
            Related opportunities
          </li>
        </ul>

        <span className="vf-breadcrumbs__heading">Related:</span>
        <ul className="vf-breadcrumbs__list vf-breadcrumbs__list--related | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/sites"
              className="vf-breadcrumbs__link"
            >
              All EMBL sites
            </a>
          </li>
        </ul>
      </nav>

      <section className="vf-intro" id="an-id-for-anchor">
        <div>{/* empty */}</div>

        <div className="vf-stack vf-stack--400">
          <h1 className="vf-intro__heading">Related opportunities</h1>
          <p className="vf-lede">
            Openings from partners and similar institutions
          </p>

          <p className="vf-intro__text">
            Job applications must not be sent to the Human Resources Section of
            EMBL but to the contact address given in the vacancy.
          </p>
          <p className="vf-intro__text">
            Please understand that EMBL cannot take any responsibility for the
            content of websites of the partner organisations nor for aspects of
            the jobs offered by them. Employment conditions for these positions
            are subject to the regulations applicable within the partner
            organisations.
          </p>
        </div>
      </section>
      <section className="embl-grid embl-grid--has-centered-content">
        <div></div>
        <div className="vf-content">
          <link
            rel="import"
            href="https://www.embl.org/api/v1/pattern.html?filter-content-type=article&filter-id=142660&pattern=node-body&source=contenthub"
            data-target="self"
            data-embl-js-content-hub-loader
          />
        </div>
      </section>
    </EMBLContentTemplate>
  );
}
