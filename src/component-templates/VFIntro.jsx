export function VfIntro({
  vf_intro_heading,
  vf_intro_subheading,
  vf_intro_badge,
  vf_intro_lede,
  vf_intro_text,
  id,
}) {
  return (
    <section className="vf-intro" id={id}>
      <div>{/* <!-- empty --> */}</div>

      <div className="vf-stack vf-stack--400">
        <h1
          className={`vf-intro__heading ${
            vf_intro_badge && "vf-intro__heading--has-tag"
          }`}
        >
          {vf_intro_heading}
          {vf_intro_badge}
        </h1>
        {vf_intro_subheading && (
          <h2 className="vf-intro__subheading">{vf_intro_subheading}</h2>
        )}

        {vf_intro_lede && <p className="vf-lede">{vf_intro_lede}</p>}

        {vf_intro_text &&
          (Array.isArray(vf_intro_text) ? (
            vf_intro_text.map((html, index) => (
              <p
                key={index}
                className="vf-intro__text"
                dangerouslySetInnerHTML={{
                  __html: html,
                }}
              ></p>
            ))
          ) : (
            <p
              className="vf-intro__text"
              dangerouslySetInnerHTML={{
                __html: vf_intro_text,
              }}
            ></p>
          ))}
      </div>
    </section>
  );
}
