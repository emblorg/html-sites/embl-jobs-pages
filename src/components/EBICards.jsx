import { VFCard } from "../component-templates/VFCard";

export function EBICards() {
  return (
    <VFCard.Container id="about">
      <VFCard
        card_image="https://acxngcvroo.cloudimg.io/v7/https://www.embl.org/files/wp-content/uploads/ebi-jobs-campus-south-building.jpg?w=500&h=300&func=crop"
        card_image__alt="Aerial image of EMBL-EBI South building on campus"
        card_heading="Life at EMBL-EBI"
        card_subheading=""
        card_text="With outstanding technical infrastructure and a flexible working style, EMBL-EBI is a medium-sized organisation with a small-company feel."
        card_href="//www.ebi.ac.uk/about/jobs/career-profiles"
        newTheme="primary"
        modifiers="vf-card--bordered"
      />
      <VFCard
        card_image="https://www.embl.org/editorhub/wp-content/uploads/2021/05/AdobeStock_136016129-768x512.jpeg?w=500&h=300&func=crop"
        card_image__alt="EMBL-EBI staff members"
        card_heading="Employee benefits"
        card_subheading=""
        card_text="We value your skills, wherever you may come from and we offer a generous benefits package to support you."
        card_href="//www.ebi.ac.uk/about/jobs/benefits"
        newTheme="primary"
        modifiers="vf-card--bordered"
      />

      <VFCard
        card_image="https://acxngcvroo.cloudimg.io/v7/https://www.embl.org/files/wp-content/uploads/AdobeStock_245533971_900x500.jpeg"
        card_image__alt="Visual of the world with connecting lines"
        card_heading="International applicants"
        card_subheading=""
        card_text="Join an inclusive community of 850+ employees from over 78 countries and get unparalleled support with visa sponsorship."
        card_href="//www.ebi.ac.uk/about/jobs/international-applicants"
        newTheme="primary"
        modifiers="vf-card--bordered"
      />
    </VFCard.Container>
  );
}
