const locations = {
  barcelona: "Barcelona",
  grenoble: "Grenoble",
  hamburg: "Hamburg",
  heidelberg: "Heidelberg",
  EMBO: "EMBO, Heidelberg",
  hinxton: "EMBL-EBI Hinxton",
  rome: "Rome",
};
const locationsWithCountries = {
  "Barcelona (Spain)": "7",
  "EMBL-EBI (Hinxton, UK)": "2",
  "EMBO (Heidelberg, Germany)": "6",
  "Grenoble (France)": "4",
  "Hamburg (Germany)": "3",
  "Heidelberg (Germany)": "1",
  "Rome (Italy)": "5",
};
const positionTypes = {
  ADM: "Administration",
  BIO: "Bioinformatics",
  COM: "Communication",
  DTC: "Data Curation",
  GEN: "General Support Services",
  ITS: "IT and Infrastructure",
  POS: "Postdoctoral Fellowships",
  PRE: "Predoctoral Fellowships",
  RES: "Research",
  FAC: "Science Faculty",
  SSS: "Scientific Services and Support",
  SDE: "Software Development and Engineering",
  TEC: "Technical Support",
  TRA: "Trainee and Internships",
  TRN: "Training",
};

export { locations, locationsWithCountries, positionTypes };
