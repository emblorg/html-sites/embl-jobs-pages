import "./VFLoader.scss";

export function VFLoader() {
  return <div className="circle-loader"></div>;
}

export function VFLoaderBox() {
  return (
    <div className="vf-box">
      <h3 className="loader-empty-box-big">
        <VFLoader />
      </h3>
    </div>
  );
}
