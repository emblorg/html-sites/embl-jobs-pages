import { EmblFooter } from "../../EmblFooter";
import { EmblHeader } from "../../EmblHeader";

export function EMBLContentTemplate({ children }) {
  return (
    <>
      <EmblHeader />

      {children}

      <EmblFooter />
    </>
  );
}
