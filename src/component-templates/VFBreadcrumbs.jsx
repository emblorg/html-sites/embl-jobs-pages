import PropTypes from "prop-types";

export function VFBreadcrumbs({ breadcrumbItems = [], relatedItems = [] }) {
  return (
    <nav className="vf-breadcrumbs" aria-label="Breadcrumb">
      <ul className="vf-breadcrumbs__list | vf-list vf-list--inline">
        {breadcrumbItems.map(({ title, link }, index) => {
          let attrs = {};
          if (index === breadcrumbItems.length - 1) {
            attrs = {
              "aria-current": "location",
            };
          }

          return (
            <li key={title} className="vf-breadcrumbs__item" {...attrs}>
              <a href={link} className="vf-breadcrumbs__link">
                {title}
              </a>
            </li>
          );
        })}
      </ul>

      {relatedItems.length ? (
        <>
          <span className="vf-breadcrumbs__heading">Related:</span>
          <ul className="vf-breadcrumbs__list vf-breadcrumbs__list--related | vf-list vf-list--inline">
            {relatedItems.map(({ title, link }) => (
              <li key={title} className="vf-breadcrumbs__item">
                <a href={link} className="vf-breadcrumbs__link">
                  {title}
                </a>
              </li>
            ))}
          </ul>
        </>
      ) : null}
    </nav>
  );
}

VFBreadcrumbs.propTypes = {
  breadcrumbItems: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      link: PropTypes.string,
    })
  ),
  relatedItems: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      link: PropTypes.string,
    })
  ),
};
