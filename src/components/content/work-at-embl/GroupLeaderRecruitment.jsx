import { ContentMeta } from "../components/ContentMeta";
import { EMBLContentTemplate } from "../components/EMBLContentTemplate";

export function GroupLeaderRecruitment() {
  return (
    <EMBLContentTemplate>
      <ContentMeta
        keywords="hr excellence, work at embl, jobs at embl, open positions at embl, jobs in biology, jobs in research, group leader at embl, team leader at embl, group leader jobs, team leader jobs, group leader opportunities, team leader opportunities"
        description="We hire group leaders early in their career and provide them with a supportive, collegial and family friendly environment that allows them to launch ambitious and original research programmes."
        canonical="https://www.embl.org/jobs/work-at-embl/group-leader-recruitment"
      />

      {/* Breadcrumbs */}
      <nav
        className="vf-breadcrumbs vf-u-margin__bottom--600"
        aria-label="Breadcrumb"
      >
        <ul className="vf-breadcrumbs__list | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/"
              className="vf-breadcrumbs__link"
            >
              Jobs
            </a>
          </li>
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/work-at-embl/"
              className="vf-breadcrumbs__link"
            >
              Careers at EMBL
            </a>
          </li>
          <li className="vf-breadcrumbs__item" aria-current="location">
            Group Leader Recruitment
          </li>
        </ul>

        <span className="vf-breadcrumbs__heading">Related:</span>
        <ul className="vf-breadcrumbs__list vf-breadcrumbs__list--related | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/sites"
              className="vf-breadcrumbs__link"
            >
              All EMBL sites
            </a>
          </li>
        </ul>
      </nav>

      {/*
  <VfIntro vf_intro_heading="Group leader recruitment"
    vf_intro_lede="We hire group leaders early in their career and provide them with a supportive, collegial and family friendly environment that allows them to launch ambitious and original research programmes."
    vf_intro_text={ "EMBL fosters excellence by providing a highly collaborative environment that benefits the research of all its scientists, whether at the graduate, post-doctoral, or group leader level."
    } />
  */}

      {/* Content */}
      <div className="vf-content">
        <link
          rel="import"
          href="https://www.embl.org/api/v1/pattern.html?filter-content-type=article&filter-id=183245&pattern=node-body&source=contenthub"
          data-target="self"
          data-embl-js-content-hub-loader
        />
      </div>

      {/* Group Leader recruitment


  <section className="embl-grid embl-grid--has-centered-content" id="group-leader-positions">
    <div className="vf-section-header">
      <h2 className="vf-section-header__heading">
        Group Leader positions{" "}
      </h2>
      <p className="vf-section-header__text">
        EMBL is recruiting highly motivated group leaders at EMBL
        Barcelona, Spain
      </p>
    </div>

    <div className="vf-content">
      <article className="vf-summary vf-summary--job">
        <h3 className="vf-summary__title">
          <a href="https://www.embl.de/jobs/searchjobs/index.php?ref=BCN00062" className="vf-summary__link">
            Group Leader – Organoids and in vitro tissues
          </a>
        </h3>
        <p className="vf-summary__text">
          EMBL Barcelona is a research unit focusing on Tissue Biology and Disease Modelling. We aim to go beyond the
          molecular and cellular scale, integrating gene circuits with a quantitative understanding of tissues and
          organs, both healthy and diseased. Developing and using in vitro 3D tissue models, such...
        </p>
        <p className="vf-summary__date">
          {" "}
          Closes 31 October 2021 | Grading: 9
        </p>
      </article>

      <article className="vf-summary vf-summary--job">
        <h3 className="vf-summary__title">
          <a href="https://www.embl.org/jobs/position/BCN00063" className="vf-summary__link">
            Group Leader – Theoretical approaches to tissue biology and disease modelling
          </a>
        </h3>
        <p className="vf-summary__text">
          At EMBL Barcelona, we go beyond the molecular and cellular scale to ask questions such as: How do thousands of
          cells dynamically self-organise to create tissues and organs? How do new emergent properties arise at the
          tissue level? Why does this sometimes go wrong, causing disease? Why do functional...
        </p>
        <p className="vf-summary__date">
          {" "}
          Closes 31 October 2021 | Grading: 9
        </p>
      </article>


    </div>

    <div className="vf-links vf-links--tight vf-links__list--s vf-links__list--very-easy">
      <h3 className="vf-links__heading">Learn more about:</h3>
      <ul className="vf-links__list vf-links__list--secondary | vf-list">
        <li className="vf-list__item">
          <a className="vf-list__link" href="/sites/barcelona/">
            EMBL Barcelona
          </a>
        </li>

        <li className="vf-list__item">
          <a className="vf-list__link" href="/research/units/tissue-biology/">
            Tissue Biology and Disease Modelling
          </a>
        </li>


        <li className="vf-list__item">
          <a className="vf-list__link" href="/topics/transversal-research-themes/">
            Transversal Research Themes
          </a>
        </li>
      </ul>
    </div>
  </section>
  */}
    </EMBLContentTemplate>
  );
}
