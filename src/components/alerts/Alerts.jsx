import { locationsWithCountries, positionTypes } from "data/jobs-data.js";
import { isEqual } from "lodash";
import { useContext, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { AppContext } from "../../app/AppContext";
import { VFBreadcrumbs } from "../../component-templates/VFBreadcrumbs";
import { EMAIL_VALIDATION_REGEX } from "../../helpers/helpers";
import { EmblFooter } from "../EmblFooter";
import { EmblHeader } from "../EmblHeader";
import "./Alerts.scss";

const locationsWithCountriesValues = Object.values(locationsWithCountries);
const positionTypesValues = Object.keys(positionTypes);

/* Subscription-alerts page, uses react-hook-form */
export function Alerts() {
  const { isEBISite } = useContext(AppContext);
  const formRef = useRef();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
    setValue,
  } = useForm({
    defaultValues: {
      loc: [],
      pos: [],
    },
  });

  const [subscriptionState, setSubscriptionState] = useState("pending");
  const [validationError, setValidationError] = useState(null);
  const [checkboxError, setCheckboxError] = useState(null);

  const onSubmit = () => {
    // formRef.current.submit();
    if (allLocationsValue.length === 0 && allPosValue.length === 0) {
      setCheckboxError(true);
      return;
    }

    const formData = new FormData(formRef.current);
    const params = new URLSearchParams(formData);
    // console.log("params", params);
    const jobUrl = `${process.env.REACT_APP_JOB_URL}${params.toString()}`;
    fetch(jobUrl, {
      method: "GET",
    })
      .then((response) => {
        if (response.status !== 200)
          throw new Error(`${response.status} ${response.statusText}`);
        setSubscriptionState("success");
      })
      .catch((error) => {
        console.log(error.message);
        setSubscriptionState("error");
        if (error.message.startsWith("422")) {
          setValidationError("Validation Error");
        }
      });
  };

  /*
   * Below code - takes ("watches") value of the loc or pos field array
   * from react-hooks-form. Then it creates an object {disabled: true/false}
   * object for spreading on the non-all checkboxes.
   * Value is based on if the first element is "0" i.e. "All xxx" selected or not
   * */

  const allLocationsValue = watch("loc");
  const isAllLocationsSelected = isEqual(
    allLocationsValue,
    locationsWithCountriesValues
  );

  const allPosValue = watch("pos");
  const isAllPositionsSelected = isEqual(allPosValue, positionTypesValues);

  return (
    <>
      <EmblHeader />
      <VFBreadcrumbs
        breadcrumbItems={[
          { title: "EMBL jobs", link: "https://www.embl.org/jobs" },
          { title: "Job alerts" },
        ]}
      />
      {subscriptionState === "success" && (
        <section className="vf-intro">
          <div></div>

          <div className="vf-stack">
            <h1 className="vf-intro__heading ">EMBL job alerts</h1>

            <p className="vf-intro__text">
              Thank you for subscribing to the EMBL job alerts.
              <br />
              <br />
              You should receive a confirmation email shortly.
              <br />
              <br />
              From now on, you'll be among the first to know about our latest
              career opportunities.
              <br />
            </p>
            {isEBISite ? (
              <a
                className="vf-button vf-button--primary"
                href="https://www.ebi.ac.uk/careers/jobs"
              >
                View jobs at EMBL-EBI
              </a>
            ) : (
              <a
                className="vf-button vf-button--primary"
                href="https://www.embl.org/jobs/"
              >
                View jobs at EMBL
              </a>
            )}
          </div>
        </section>
      )}
      {subscriptionState === "error" && (
        <section className="vf-intro">
          <div></div>

          <div className="vf-stack">
            <h1 className="vf-intro__heading ">Error</h1>
            {validationError ? (
              <>
                <p className="vf-lede">There was a validation error.</p>
                <p className="vf-intro__text">
                  Please check your details and try again.
                </p>
              </>
            ) : (
              <>
                <p className="vf-lede">There was a technical error.</p>
                <p className="vf-intro__text">
                  This problem means that the service you are trying to access
                  currently unavailable. Please try again later.
                </p>
              </>
            )}
          </div>
        </section>
      )}

      {subscriptionState === "pending" && (
        <>
          <section className="vf-intro" id="an-id-for-anchor">
            <div>{/* empty */}</div>

            <div className="vf-stack vf-stack--400">
              <h1 className="vf-intro__heading ">EMBL job alerts</h1>

              <p className="vf-lede">Get the latest postings in your inbox.</p>

              <p className="vf-intro__text">
                Configure your criteria and add your email address to configure
                your alert.{" "}
              </p>
            </div>
          </section>
          <section className="vf-intro | embl-grid embl-grid--has-centered-content">
            <div></div>
            <div className="vf-content">
              <form
                action="https://www.embl.de/jobs/searchjobs/index.php"
                method="GET"
                className="vf-form"
                ref={formRef}
                onSubmit={handleSubmit(onSubmit)}
              >
                <input type="hidden" name="newlang" value="1" />

                <label htmlFor="email_alert" className="vf-form__label">
                  Email address
                </label>
                <input
                  type="text"
                  {...register("email_alert", {
                    required: true,
                    pattern: EMAIL_VALIDATION_REGEX,
                  })}
                  id="email_alert"
                  className={`vf-form__input ${
                    errors.email_alert && "vf-form__input--invalid"
                  }`}
                  placeholder="email@youraddress.net"
                />

                {errors.email_alert && (
                  <p className="vf-form__helper vf-form__helper--error">
                    {errors.email_alert.type === "required"
                      ? "Email address is required."
                      : "Invalid email address."}
                  </p>
                )}

                <br />

                <fieldset className="vf-form__fieldset vf-u-margin__bottom--800">
                  <legend className="form-subtitle vf-form__legend">
                    Location
                  </legend>
                  <div className="vf-u-margin__top--200 vf-u-margin__bottom--400">
                    {!isAllLocationsSelected && (
                      <button
                        className="vf-link link-button"
                        onClick={() => {
                          setValue("loc", locationsWithCountriesValues);
                        }}
                      >
                        Select all locations
                      </button>
                    )}
                    {isAllLocationsSelected && (
                      <button
                        className="vf-link link-button"
                        onClick={() => {
                          setValue("loc", []);
                        }}
                      >
                        Clear all locations
                      </button>
                    )}
                  </div>
                  <div className="grid">
                    <ul className="vf-list">
                      {Object.entries(locationsWithCountries).map(
                        ([displayText, locationValue]) => (
                          <li
                            key={locationValue}
                            className="vf-form__item vf-form__item--checkbox"
                          >
                            <input
                              type="checkbox"
                              className="vf-form__checkbox"
                              id={"loc" + locationValue}
                              value={locationValue}
                              {...register("loc[]")}
                            />
                            <label
                              htmlFor={"loc" + locationValue}
                              className="vf-form__label"
                            >
                              {displayText}
                            </label>
                          </li>
                        )
                      )}
                    </ul>
                  </div>
                </fieldset>

                <fieldset className="fieldset-job vf-form__fieldset">
                  <legend className="form-subtitle vf-form__legend">
                    Categories
                  </legend>
                  <div className="vf-u-margin__top--200 vf-u-margin__bottom--400">
                    {!isAllPositionsSelected && (
                      <button
                        className="vf-link link-button"
                        onClick={() => {
                          setValue("pos", positionTypesValues);
                        }}
                      >
                        Select all categories
                      </button>
                    )}
                    {isAllPositionsSelected && (
                      <button
                        className="vf-link link-button"
                        onClick={() => {
                          setValue("pos", []);
                        }}
                      >
                        Clear all categories
                      </button>
                    )}
                  </div>
                  <div className="grid">
                    <ul className="vf-list">
                      {Object.entries(positionTypes).map(
                        ([value, displayText], index) => (
                          <li
                            key={value}
                            className="vf-form__item vf-form__item--checkbox"
                          >
                            <input
                              type="checkbox"
                              className="vf-form__checkbox"
                              id={"pos" + index}
                              value={value}
                              {...register("pos[]")}
                            />
                            <label
                              htmlFor={"pos" + index}
                              className="vf-form__label"
                            >
                              {displayText}
                            </label>
                          </li>
                        )
                      )}
                    </ul>
                  </div>
                </fieldset>
                <input type="hidden" name="srch_trm" value="" />
                <input
                  type="hidden"
                  name="create_job_alert"
                  value="Create job alert"
                />
                {checkboxError && (
                  <p className="vf-form__helper vf-form__helper--error">
                    Please select a location or category.
                  </p>
                )}
                <div className="buttons clearfix vf-u-margin__top--800">
                  <input
                    type="submit"
                    id="jobs_alert_btn"
                    className="vf-button vf-button--primary"
                    value="Create job alert"
                    title="Create job alert"
                  />
                </div>
              </form>
            </div>
          </section>
        </>
      )}
      <EmblFooter />
    </>
  );
}
