import { ContentMeta } from "./components/ContentMeta";
import { EMBLContentTemplate } from "./components/EMBLContentTemplate";

export function HrExcellenceInResearch() {
  return (
    <EMBLContentTemplate>
      <ContentMeta
        keywords="jobs, hr excellence, work at embl, jobs at embl, jobs, open positions, jobs in biology, jobs in research"
        description="EMBL has been conferred with a badge of ‘excellence’ by the European Commission, in recognition of its progress in implementing the European Charter for Researchers and the Code of Conduct for Recruitment of Researchers."
        canonical="https://www.embl.org/jobs/hr-excellence-in-research"
      />

      {/* Breadcrumbs */}
      <nav
        className="vf-breadcrumbs vf-u-margin__bottom--600"
        aria-label="Breadcrumb"
      >
        <ul className="vf-breadcrumbs__list | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/"
              className="vf-breadcrumbs__link"
            >
              Jobs
            </a>
          </li>
          <li className="vf-breadcrumbs__item" aria-current="location">
            HR Excellence in Research
          </li>
        </ul>

        <span className="vf-breadcrumbs__heading">Related:</span>
        <ul className="vf-breadcrumbs__list vf-breadcrumbs__list--related | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/sites"
              className="vf-breadcrumbs__link"
            >
              All EMBL sites
            </a>
          </li>
        </ul>
      </nav>

      {/* Content */}
      <div className="vf-content">
        <link
          rel="import"
          href="https://www.embl.org/api/v1/pattern.html?filter-content-type=article&filter-id=183263&pattern=node-body&source=contenthub"
          data-target="self"
          data-embl-js-content-hub-loader
        />
      </div>
    </EMBLContentTemplate>
  );
}
