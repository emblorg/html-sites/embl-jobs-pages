import { VFCard } from "../component-templates/VFCard";

export function EmblCards() {
  return (
    <VFCard.Container>
      <VFCard
        card_image="https://acxngcvroo.cloudimg.io/v7/https://www.embl.org/files/wp-content/uploads/2020/04/0607_ells-school-group_hd-s.jpg"
        card_image__alt="People looking at a test"
        card_heading="Careers at EMBL"
        card_subheading=""
        card_text="Join a community of world-class researchers and skilled professionals working together to uncover the secrets of life. We offer attractive conditions and benefits to attract and retain the brightest talent."
        card_href="/jobs/work-at-embl/"
        newTheme="primary"
        modifiers="vf-card--bordered"
      />
      <VFCard
        card_image="https://acxngcvroo.cloudimg.io/v7/www.embl.org/files/wp-content/uploads/20210719_Sites_Composition-s.jpg"
        card_image__alt="EMBL sites composition"
        card_heading="One laboratory. Six sites. Global reach."
        card_subheading=""
        card_text="The European Molecular Biology Laboratory is a single organisation spread across six European sites. Each site hosts its own research units, services, and facilities."
        card_href="//www.embl.org/sites/"
        newTheme="primary"
        modifiers="vf-card--bordered"
      />

      <VFCard
        card_image="https://acxngcvroo.cloudimg.io/v7/https://www.embl.org/files/wp-content/uploads/20210426_EMBL_About_3000x1000-scaled.jpg?w=800&h=550&gravity=auto"
        card_image__alt="People talking at an EMBL event coffee break"
        card_heading="EMBL is Europe’s life sciences laboratory"
        card_subheading=""
        card_text="We conduct world-class excellent biological research, provide training for students and scientists, and provide state-of-the-art technologies for a wide range of scientific and experimental services."
        card_href="//www.embl.org/about/"
        newTheme="primary"
        modifiers="vf-card--bordered"
      />
    </VFCard.Container>
  );
}
