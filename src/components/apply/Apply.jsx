import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { EmblBreadcrumbs } from "../EmblBreadcrumbs";
import { EmblFooter } from "../EmblFooter";
import { EmblHeader } from "../EmblHeader";
import { ApplyHeader } from "../ApplyHeader";

export function Apply() {
  return (
    <>
      <ApplyHeader />
      <EmblHeader />
      <EmblBreadcrumbs />
      <section className="embl-grid embl-grid--has-centered-content">
        <Helmet>
          <script
            type="text/template"
            data-template="TextInput"
            src={process.env.PUBLIC_URL + "/assets/_TextInput.html"}
          ></script>
          <script
            type="text/template"
            data-template="PhoneNumberInput"
            src={process.env.PUBLIC_URL + "/assets/_PhoneNumberInput.html"}
          ></script>
          <script
            type="text/template"
            data-template="DateSelect"
            src={process.env.PUBLIC_URL + "/assets/_DateSelect.html"}
          ></script>
          <script
            type="text/template"
            data-template="SelectDropDown"
            src={process.env.PUBLIC_URL + "/assets/_SelectDropDown.html"}
          ></script>
          {/* CTS Default / Branded Bootstrap  */}
          <link
            href="https://emea3.recruitmentplatform.com/apply-app/static/vanilla-silk/release/1-LATEST/css/cmp-bs.css"
            rel="stylesheet"
          />
          {/* CTS / Cascada Main Silk Style  */}
          <link
            href="https://emea3.recruitmentplatform.com/apply-app/static/vanilla-silk/release/1-LATEST/css/main.css"
            rel="stylesheet"
          />
          <link
            href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            rel="stylesheet"
            integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
            crossOrigin="anonymous"
          />
          <link
            rel="stylesheet"
            type="text/css"
            href={process.env.PUBLIC_URL + "/assets/ie-fixes.css"}
          />
          <link
            rel="stylesheet"
            type="text/css"
            href={process.env.PUBLIC_URL + "/assets/custom.css"}
          />

          <script
            type="text/javascript"
            src="https://emea3.recruitmentplatform.com/apply-app/static/apply/release/2-LATEST/apply-application-form-namespaced.js"
          ></script>

          {/* <script
            type="text/javascript"
            src={process.env.PUBLIC_URL + "/assets/main.js"}
          ></script> */}
          {/* <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossOrigin="anonymous"
          ></script> */}
          <script
            src={process.env.PUBLIC_URL + "/assets/talenstplace-alert.min.js"}
            type="text/javascript"
          ></script>
          {/* <script
            src={process.env.PUBLIC_URL + "/assets/custom.js"}
            type="text/javascript"
          ></script> */}
          {/* Apply */}
          {/* <script
            type="text/javascript"
            src="https://emea3.recruitmentplatform.com/apply-app/static/apply/release/2-LATEST/apply-preloader-namespaced.js"
            data-lumesse-apply
            data-lumesse-apply-config-key="AAAC8QAA-1d049852-a5bd-42da-bd30-ebe6eec9b143"
            data-lumesse-apply-host="https://emea3.recruitmentplatform.com"
            data-lumesse-apply-errors-placement="bottom"
            data-lumesse-apply-description-placement="bottom"
            data-lumesse-apply-menu-placement="left"
          ></script> */}
        </Helmet>
        <div>{/* {/* empty */} </div>

        <div className="vf-content">
          <main className="" id="vanillaApplyContent">
            <section id="apply-details">
              <div data-lumesse-apply-resume className="hidden"></div>
              <div
                data-lumesse-apply-container
                data-lumesse-apply-description-placement="bottom"
                data-lumesse-apply-errors-placement="bottom"
                className="tsApplyContainer"
              ></div>
              <div data-lumesse-apply-submission-accepted className=""></div>
            </section>
          </main>
        </div>
      </section>
      <section className="embl-grid embl-grid--has-centered-content">
        <div>{/* {/* empty */} </div>
        <div className="vf-content">
          <hr className="vf-divider" />
          {/* related links */}

          <div className="vf-links vf-links--tight vf-links__list--s vf-links__list--very-easy">
            <ul className="vf-links__list vf-links__list--secondary | vf-list">
              <li className="vf-list__item">
                <Link className="vf-list__link" to="/">
                  View all EMBL jobs
                </Link>
              </li>
              <li className="vf-list__item">
                <Link className="vf-list__link" to="/alerts">
                  Sign up for job alerts
                </Link>
              </li>
              <li className="vf-list__item">
                <Link className="vf-list__link" to="/partners">
                  View openings from partners
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <EmblFooter />
    </>
  );
}
