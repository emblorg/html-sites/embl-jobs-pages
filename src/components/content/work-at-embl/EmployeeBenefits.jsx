import { ContentMeta } from "../components/ContentMeta";
import { EMBLContentTemplate } from "../components/EMBLContentTemplate";

export function EmployeeBenefits() {
  return (
    <EMBLContentTemplate>
      <ContentMeta
        keywords="basic research, conditions of employment, financial support, remunaration, family life, visa requiremnts, social security, helath insurace, health insurace package, working hours, flexible workign arrangements, hybrid workig, training, career developmemnt, professional development, work at embl, jobs at embl"
        description="We value your skills and offer a generous benefits package to support you and your family."
        canonical="https://www.embl.org/jobs/work-at-embl/benefits"
      />

      {/* Breadcrumbs */}
      <nav className="vf-breadcrumbs" aria-label="Breadcrumb">
        <ul className="vf-breadcrumbs__list | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/"
              className="vf-breadcrumbs__link"
            >
              Jobs
            </a>
          </li>
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/work-at-embl/"
              className="vf-breadcrumbs__link"
            >
              Careers at EMBL
            </a>
          </li>
          <li className="vf-breadcrumbs__item" aria-current="location">
            Employee Benefits
          </li>
        </ul>

        <span className="vf-breadcrumbs__heading">Related:</span>
        <ul className="vf-breadcrumbs__list vf-breadcrumbs__list--related | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/sites"
              className="vf-breadcrumbs__link"
            >
              All EMBL sites
            </a>
          </li>
        </ul>
      </nav>

      {/* Content */}
      <div className="vf-content">
        <link
          rel="import"
          href="https://www.embl.org/api/v1/pattern.html?filter-content-type=article&filter-id=178941&pattern=node-body&source=contenthub"
          data-target="self"
          data-embl-js-content-hub-loader
        />
      </div>
    </EMBLContentTemplate>
  );
}
