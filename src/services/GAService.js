import { vfGaIndicateLoaded, vfGaTrackInteraction } from "@visual-framework/vf-analytics-google/vf-analytics-google.js";

class GAService {
  constructor() {
    if (GAService._instance) {
      throw new Error('Cannot be instantiated more than once');
    }
    GAService._instance = this;
    // declare this function here to prevent being used again
    const injectGAScript = (trackingId) => {
      const gaInitScript = `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '${trackingId}');
      `;
      // inject gtag script
      const scriptGTAG = document.createElement('script');
      scriptGTAG.src = `https://www.googletagmanager.com/gtag/js?id=${trackingId}`;
      scriptGTAG.async = true;
      document.getElementsByTagName('head')[0].appendChild(scriptGTAG);

      // create the script
      const scriptElement = document.createElement('script');
      scriptElement.id = 'ga-init-script';
      scriptElement.type = 'text/javascript';
      scriptElement.text = gaInitScript;

      document.getElementsByTagName('head')[0].appendChild(scriptElement);
    }
    const GATrackingId = process.env.REACT_APP_GA_TRACKING_ID;
    injectGAScript(GATrackingId);
    const vfGaTrackOptions = {
      vfGaTrackPageLoad: true,
      vfGa4MeasurementId: GATrackingId,
      vfGaTrackNetwork: {
        serviceProvider: "dimension2",
        networkDomain: "dimension3",
        networkType: "dimension4"
      }
    };
    vfGaIndicateLoaded(vfGaTrackOptions);

    if (process.env.REACT_APP_WEB_ENV === "development") {
      document
      .getElementsByTagName("body")[0]
      .setAttribute("data-vf-google-analytics-verbose", "true");
    }
  }
  trackEvent(element, customEventName) {
    vfGaTrackInteraction(element, customEventName);
  }
}

const service = new GAService();
export default service;