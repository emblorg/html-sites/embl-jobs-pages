import { EmblCards } from "../EmblCards";
import { EmblFooter } from "../EmblFooter";
import { EmblHeader } from "../EmblHeader";
import { Home } from "./Home";
import "./Home.scss";
import { VFBreadcrumbs } from "../../component-templates/VFBreadcrumbs";

export function EMBLHome() {
  return (
    <>
      <EmblHeader />
      <VFBreadcrumbs
        breadcrumbItems={[{ title: "Jobs", link: "https://www.embl.org/jobs" }]}
        relatedItems={[
          { title: "All EMBL sites", link: "https://www.embl.org/sites" },
        ]}
      />

      {/* notification for GL recruitment

      <section className="vf-banner vf-banner--alert vf-banner--info vf-u-margin__bottom--400 vf-u-margin__top--400">
        <div className="vf-banner__content">
          <p className="vf-banner__text">
            EMBL is recruiting highly motivated group leaders at EMBL
            Barcelona.{" "}
            <a
              className="vf-banner__link"
              href="/jobs/work-at-embl/group-leader-recruitment/#group-leader-positions"
            >
              Learn more
            </a>
          </p>
        </div>
      </section>

      */}

      <section className="vf-hero | vf-u-fullbleed">
        <div className="vf-hero__content | vf-box | vf-stack vf-stack--400">
          {" "}
          <h1 className="vf-hero__heading">
            <a className="vf-hero__heading_link">Opportunities across EMBL</a>
          </h1>
          <p className="vf-hero__subheading">
            We're a community of world-class researchers and skilled
            professionals working together to uncover the secrets of life.
          </p>{" "}
          <p className="vf-hero__text">
            We offer{" "}
            <a className="vf-link" href="/jobs/work-at-embl">
              attractive conditions and benefits
            </a>{" "}
            to attract and retain the brightest talent for scientific and
            non-scientific positions. We encourage applications from
            international candidates at all career levels.
          </p>
        </div>
      </section>

      <Home />

      <div className="vf-section-header vf-u-margin__bottom--800">
        <h2 className="vf-section-header__heading">Find more opportunities:</h2>
      </div>

      <section className="vf-grid vf-grid__col-3 vf-u-margin__bottom--1200">
        <article className="vf-card vf-card--brand vf-card--striped">
          <div className="vf-card__content | vf-stack vf-stack--400">
            <h3 className="vf-card__heading">
              <a
                className="vf-card__link"
                href="https://www.embl.org/about/info/embl-international-phd-programme/application/"
              >
                Predoctoral fellowships
                <svg
                  aria-hidden="true"
                  className="vf-card__heading__icon | vf-icon vf-icon-arrow--inline-end"
                  width="1em"
                  height="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0 12c0 6.627 5.373 12 12 12s12-5.373 12-12S18.627 0 12 0C5.376.008.008 5.376 0 12zm13.707-5.209l4.5 4.5a1 1 0 010 1.414l-4.5 4.5a1 1 0 01-1.414-1.414l2.366-2.367a.25.25 0 00-.177-.424H6a1 1 0 010-2h8.482a.25.25 0 00.177-.427l-2.366-2.368a1 1 0 011.414-1.414z"
                    fill="currentColor"
                    fillRule="nonzero"
                  ></path>
                </svg>
              </a>
            </h3>
          </div>
        </article>

        <article className="vf-card vf-card--brand vf-card--striped">
          <div className="vf-card__content | vf-stack vf-stack--400">
            <h3 className="vf-card__heading">
              <a
                className="vf-card__link"
                href="https://www.embl.org/about/info/postdoctoral-programme/"
              >
                Postdoctoral fellowships
                <svg
                  aria-hidden="true"
                  className="vf-card__heading__icon | vf-icon vf-icon-arrow--inline-end"
                  width="1em"
                  height="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0 12c0 6.627 5.373 12 12 12s12-5.373 12-12S18.627 0 12 0C5.376.008.008 5.376 0 12zm13.707-5.209l4.5 4.5a1 1 0 010 1.414l-4.5 4.5a1 1 0 01-1.414-1.414l2.366-2.367a.25.25 0 00-.177-.424H6a1 1 0 010-2h8.482a.25.25 0 00.177-.427l-2.366-2.368a1 1 0 011.414-1.414z"
                    fill="currentColor"
                    fillRule="nonzero"
                  ></path>
                </svg>
              </a>
            </h3>
          </div>
        </article>

        <article className="vf-card vf-card--brand vf-card--striped">
          <div className="vf-card__content | vf-stack vf-stack--400">
            <h3 className="vf-card__heading">
              <a className="vf-card__link" href="/jobs/partners/">
                Related opportunities
                <svg
                  aria-hidden="true"
                  className="vf-card__heading__icon | vf-icon vf-icon-arrow--inline-end"
                  width="1em"
                  height="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0 12c0 6.627 5.373 12 12 12s12-5.373 12-12S18.627 0 12 0C5.376.008.008 5.376 0 12zm13.707-5.209l4.5 4.5a1 1 0 010 1.414l-4.5 4.5a1 1 0 01-1.414-1.414l2.366-2.367a.25.25 0 00-.177-.424H6a1 1 0 010-2h8.482a.25.25 0 00.177-.427l-2.366-2.368a1 1 0 011.414-1.414z"
                    fill="currentColor"
                    fillRule="nonzero"
                  ></path>
                </svg>
              </a>
            </h3>
          </div>
        </article>
      </section>

      <hr className="vf-divider" />

      <EmblCards />

      <EmblFooter />
    </>
  );
}
