import BioInformaticsMateus from "images/patricio-mateus.jpg";
import { parseGrade } from "../../helpers/helpers";

const TestimonyComponent = ({ image, quote, author }) => {
  return (
    <section
      className="embl-grid embl-grid--has-sidebar"
      data-vf-google-analytics-region="description-staff-image"
      itemProp="description"
    >
      <div>{/* empty */}</div>
      <div className="vf-content">
        <figure className="vf-figure">
          <img
            className="vf-figure__image"
            src={image}
            alt="Image of EMBL-EBI staff member at her desk"
            loading="lazy"
          />
          <figcaption className="vf-figure__caption vf-blockquote">
            "{quote}"
            <br />
            {author}
          </figcaption>
        </figure>
      </div>
    </section>
  );
};

const AndyYatesTestimony = (
  <TestimonyComponent
    image={
      "https://www.ebi.ac.uk/sites/ebi.ac.uk/files/groups/external_relations/images/Jobs/YATES_Andy_DiNA_EMBL-EBI_2016.jpg"
    }
    quote="We have a lot of smart people here. I really enjoy making sure they have the right environment to maximise their potential."
    author="Andy Yates, Team Leader"
  />
);

export default function Testimony({ job = {} }) {
  const { field_jobs_type: jobType } = job;

  // format of job grade string is "5 - 6 (monthly salary starting at $1234)". This converts it into a number
  const jobGrade = parseGrade(job.field_jobs_advertise_grade);

  // only if its a leader and grade is >= 9
  if (jobType.includes("Science Faculty") && jobGrade >= 9) {
    return AndyYatesTestimony;
  }

  if (jobType.includes("Software Development")) {
    // Grade 4, 5
    if (jobGrade === 4 || jobGrade === 5) {
      return (
        <TestimonyComponent
          image={
            "https://acxngcvroo.cloudimg.io/v7/https://www.embl.org/files/wp-content/uploads/THORMAN_Anja_EMBL-EBI_2016.jpeg.jpg"
          }
          quote="Since my first day at EMBL-EBI, I have cherished how friendly,
                helpful and talented everyone is. I particularly like the lively
                exchange of knowledge on campus."
          author="Anja Thormann, Software Developer, Variation team"
        />
      );
    } else if (jobGrade === 6 || jobGrade === 7) {
      return (
        <TestimonyComponent
          image={
            "https://acxngcvroo.cloudimg.io/v7/https://www.ebi.ac.uk/sites/ebi.ac.uk/files/groups/external_relations/images/Jobs/HAWKINS_Ken_EMBL-EBI_2016.jpg"
          }
          quote="There’s a lot of opportunity to design for the need of content
                and users, and to use web tools to understand how users engage
                with content."
          author="Ken Hawkins, Web Designer"
        />
      );
    } else if (jobGrade === 8) {
      return AndyYatesTestimony;
    }
  }
  if (jobType.includes("IT and Infrastructure")) {
    return (
      <TestimonyComponent
        image={
          "https://acxngcvroo.cloudimg.io/v7/https://www.ebi.ac.uk/sites/ebi.ac.uk/files/groups/communications/Tim%20Dyce%20copy%20770x289.jpg"
        }
        quote="What EMBL-EBI does is extremely positive and I wanted to be a
              part of that. Throughout my career, I’ve always tried to take on
              roles where I can go home in the evenings feeling I have done
              something to support an organisation that seeks to be altruistic."
        author="Tim Dyce, Head of Infrastructure Services"
      />
    );
  }
  if (jobType.includes("Bioinformatics")) {
    if (jobGrade === 5 || jobGrade === 6) {
      return (
        <TestimonyComponent
          image={BioInformaticsMateus}
          quote="I often find myself involved with completely new tasks that
              require me to be dynamic and learn a whole new set of skills. You
              arrive here with a set of skills and knowledge, but you will
              probably leave having learned a lot more."
          author="Mateus Patricio, Bioinformatician"
        />
      );
    } else if (jobGrade === 7 || jobGrade === 8) {
      return AndyYatesTestimony;
    }
  }
  if (
    jobType.includes("Postdoctoral Fellowship") ||
    jobType.includes("Predoctoral Fellowship")
  ) {
    return (
      <TestimonyComponent
        image={
          "https://www.ebi.ac.uk/sites/ebi.ac.uk/files/resize/groups/external_relations/images/People/Sara_Saheb_Kashaf_PhD1-770x289.jpg"
        }
        quote="Environment matters a lot more than you think. I picked my
              supervisors primarily based on the research coming out of their
              groups. I didn’t expect that they would also turn out to be
              amazing mentors."
        author="Sara Saheb Kashaf, PhD student"
      />
    );
  }
  if (jobType.includes("Administration")) {
    return (
      <TestimonyComponent
        image={
          "https://www.ebi.ac.uk/sites/ebi.ac.uk/files/groups/external_relations/images/People/Gemma_Wood_770x400.jpeg"
        }
        quote="People get satisfaction out of seeing their work make a difference, so I encourage my team to look at outcomes and impact."
        author="Gemma Wood, Head of Communications"
      />
    );
  }
  return null;
}
