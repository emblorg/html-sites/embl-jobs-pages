import { ContentMeta } from "./components/ContentMeta";
import { EMBLContentTemplate } from "./components/EMBLContentTemplate";

export function Science4Refugees() {
  return (
    <EMBLContentTemplate>
      <ContentMeta
        keywords="jobs, hr excellence, work at embl, jobs at embl, jobs, open positions, jobs in biology, jobs in research, Science4Refugees initiative, european comission"
        description="EMBL welcomes applications from talented refugees and asylum seekers who have a scientific background and who would like to apply for our advertised positions, including internships, part-time and full-time jobs."
        canonical="https://www.embl.org/jobs/science4refugees/"
      />

      {/*
  <VfIntro vf_intro_heading="Science4Refugees"
    vf_intro_lede="The European Commission has launched the Science4Refugees initiative to help refugee scientists and researchers find suitable jobs that both improve their own situation and put their skills and experience to good use in Europe's research system." />
  */}

      {/* Breadcrumbs */}
      <nav
        className="vf-breadcrumbs vf-u-margin__bottom--600"
        aria-label="Breadcrumb"
      >
        <ul className="vf-breadcrumbs__list | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/"
              className="vf-breadcrumbs__link"
            >
              Jobs
            </a>
          </li>
          <li className="vf-breadcrumbs__item" aria-current="location">
            Science4Refugees
          </li>
        </ul>

        <span className="vf-breadcrumbs__heading">Related:</span>
        <ul className="vf-breadcrumbs__list vf-breadcrumbs__list--related | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/sites"
              className="vf-breadcrumbs__link"
            >
              All EMBL sites
            </a>
          </li>
        </ul>
      </nav>

      {/* Content */}
      <div className="vf-content">
        <link
          rel="import"
          href="https://www.embl.org/api/v1/pattern.html?filter-content-type=article&filter-id=183335&pattern=node-body&source=contenthub"
          data-target="self"
          data-embl-js-content-hub-loader
        />
      </div>
    </EMBLContentTemplate>
  );
}
