const { useEffect } = require("react");

// Call this hook to enable scroll to top on load functionality
export function useScrollToTop() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
}
