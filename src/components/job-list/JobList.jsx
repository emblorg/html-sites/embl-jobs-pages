import { VFLoaderBox } from "component-templates/VFLoader/VFLoader";
import { formatISODate, parseISODateFromTimeHtml } from "helpers/date-helpers";
import { Link } from "react-router-dom";
import "./JobList.scss";

export function JobList({ jobs, loading, resetFilters, isIframe = false }) {
  if (loading) {
    return <VFLoaderBox />;
  }

  return (
    <div
      className="vf-content"
      data-vf-google-analytics-region="jobs-vacancies-search-results"
    >
      {jobs.map((job) => (
        <JobList.JobListItem
          key={job.field_jobs_reference_number}
          job={job}
          isIframe={isIframe}
        />
      ))}
      {!jobs.length && (
        <p>
          No matching jobs found;{" "}
          <a href="#" onClick={resetFilters}>
            Reset your filters
          </a>
          .
        </p>
      )}
      <hr className="vf-divider" />

      {!isIframe && (
        <p>
          <Link to="/alerts">Create a customised email alert for jobs</Link>
        </p>
      )}
    </div>
  );
}

JobList.JobListItem = function JobListItem({ job, isIframe = false }) {
  return (
    <article
      key={job.id}
      className="vf-summary vf-summary--job | jplist-text-area"
    >
      <h3 className="job-title vf-summary__title">
        <Link
          to={{
            pathname: `/position/${job.field_jobs_reference_number}`,
            state: { job },
          }}
          target={isIframe ? "_parent" : "_self"}
          className="vf-summary__link"
          dangerouslySetInnerHTML={{ __html: job?.title }}
        ></Link>
      </h3>

      <p className="vf-summary__meta location">
        {job.field_jobs_staff_category}{" "}
        {job.field_jobs_group === "EMBO" ? (
          <>
            at <span className="jplist-duty-station">EMBO</span> in Heidelberg
          </>
        ) : (
          <>at {job.field_jobs_duty_station.replace(/<[^>]+>/g, "")}</>
        )}
      </p>

      <span className="jplist-job-type | vf-u-display-none">
        {job.field_jobs_type_key}
      </span>
      <p className="vf-summary__date">
        Closes{" "}
        {formatISODate(parseISODateFromTimeHtml(job.field_jobs_expiration))}
        &nbsp;|&nbsp; Grading: {job.field_jobs_advertise_grade}
      </p>
    </article>
  );
};
