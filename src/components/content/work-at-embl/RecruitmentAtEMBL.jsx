import { ContentMeta } from "../components/ContentMeta";
import { EMBLContentTemplate } from "../components/EMBLContentTemplate";

export function RecruitmentAtEMBL() {
  return (
    <EMBLContentTemplate>
      <ContentMeta
        keywords="careers at embl, work at embl, jobs at embl, recruitment"
        description="Everything you need to know about the EMBL recruitment process, each step of the way."
        canonical="https://www.embl.org/jobs/work-at-embl/recruitment"
      />

      {/* Breadcrumbs */}
      <nav
        className="vf-breadcrumbs vf-u-margin__bottom--600"
        aria-label="Breadcrumb"
      >
        <ul className="vf-breadcrumbs__list | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/"
              className="vf-breadcrumbs__link"
            >
              Jobs
            </a>
          </li>
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/work-at-embl/"
              className="vf-breadcrumbs__link"
            >
              Careers at EMBL
            </a>
          </li>
          <li className="vf-breadcrumbs__item" aria-current="location">
            Recruitment process
          </li>
        </ul>

        <span className="vf-breadcrumbs__heading">Related:</span>
        <ul className="vf-breadcrumbs__list vf-breadcrumbs__list--related | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/sites"
              className="vf-breadcrumbs__link"
            >
              All EMBL sites
            </a>
          </li>
        </ul>
      </nav>

      {/* Content */}
      <div className="vf-content">
        <link
          rel="stylesheet"
          href="https://assets.emblstatic.net/vf/v2.5.14/css/styles.css"
        />
        <link
          rel="import"
          href="https://www.embl.org/api/v1/pattern.html?filter-content-type=article&filter-id=178873&pattern=node-body&source=contenthub"
          data-target="self"
          data-embl-js-content-hub-loader
        />
      </div>
    </EMBLContentTemplate>
  );
}
