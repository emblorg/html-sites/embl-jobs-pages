import { EmblBreadcrumbs } from "../../components/EmblBreadcrumbs";
import { EmblFooter } from "../../components/EmblFooter";
import { EmblHeader } from "../../components/EmblHeader";
import { VfIntro } from "../VFIntro";

export function ErrorPage404() {
  return (
    <>
      <EmblHeader />
      <EmblBreadcrumbs />
      <VfIntro
        vf_intro_heading="Error: 404"
        vf_intro_subheading=""
        vf_intro_lede="We’re sorry - we can’t find the page or file you requested."
        vf_intro_text={[
          "It may have been removed, had its name changed, or be temporarily unavailable.",
          "You might try <a href='https://www.embl.org/search'>searching for it</a>.",
        ]}
      />

      <section className="embl-grid">
        <div></div>
        <div className="vf-content">
          <h3>Need assistance?</h3>
          <a
            className="vf-button vf-button--primary"
            href="https://www.embl.org/contact/websupport/"
          >
            Contact web support
          </a>
        </div>
      </section>
      <EmblFooter />
    </>
  );
}
