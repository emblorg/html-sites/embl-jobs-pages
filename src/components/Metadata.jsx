import { merge } from "lodash-es";
import { useMemo } from "react";
import { Helmet } from "react-helmet";

import defaultMetadata from "../data/metadata.json";

/* Adds metadata to header of page, provide metadata prop object to override defaults. */
export function Metadata({ metadata = {}, page = {} }) {
  const finalMetadata = useMemo(
    () => merge(defaultMetadata, metadata),
    [metadata]
  );

  const { meta, site_information, embl_content_meta_properties } =
    finalMetadata;

  return (
    <Helmet>
      <title>{site_information.title || "Jobs | EMBL"}</title>
      {/* https://support.google.com/webmasters/answer/139066?hl=en */}
      {meta.canonical && <link rel="canonical" href={meta.canonical} />}
      {/* <!-- Search indexing optimisations --> */}
      <meta
        className="swiftype"
        name="what"
        data-type="string"
        content={embl_content_meta_properties.what}
      />
      {/* https://swiftype.com/documentation/site-search/crawler-configuration/meta-tags#thumbnails */}
      {meta.swiftype_image && (
        <meta
          className="swiftype"
          name="image"
          data-type="enum"
          content={meta.swiftype_image}
        />
      )}
      {/* <!-- Descriptive meta --> */}
      <meta name="title" content={site_information.title} />
      <meta name="author" content={meta.author || "EMBL"} />
      <meta name="keywords" content={meta.keywords} />
      <meta name="description" content={meta.description} />
      {/* <!-- Open Graph / Facebook --> */}
      <meta property="og:type" content="website" />
      <meta
        property="og:url"
        content={`https://embl.org/jobs${page.url || ""}`}
      />
      <meta property="og:title" content={site_information.title} />
      <meta property="og:description" content={meta.description} />
      {meta.image && <meta name="og:image" content={meta.image} />}
      {/* <!-- Twitter --> */}
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="og:url" content={`https://embl.org/jobs${page.url || ""}`} />
      <meta name="twitter:title" content={site_information.title} />
      <meta name="twitter:description" content={meta.description} />
      {meta.image && <meta name="twitter:image" content={meta.image} />}
      {/* <!-- Content descriptors --> */}
      {/* the people, groups and teams involved */}
      <meta name="embl:who" content={embl_content_meta_properties.who} />
      {/* at which EMBL sites the content applies */}
      <meta name="embl:where" content={embl_content_meta_properties.where} />
      {/* the activities covered */}
      <meta name="embl:what" content={embl_content_meta_properties.what} />
      {/* which of the who/what/where is active */}
      <meta name="embl:active" content={embl_content_meta_properties.active} />
      {/* <!-- Content role --> */}
      {/* if content is task and work based or if is meant to inspire */}
      <meta
        name="embl:utility"
        content={embl_content_meta_properties.utility}
      />
      {/* if content is externally (public) or internally focused (those that work at EMBL) */}
      <meta name="embl:reach" content={embl_content_meta_properties.reach} />
      {/* <!-- Page information --> */}
      {/* the contact person or group responsible for the page */}
      <meta
        name="embl:maintainer"
        content={embl_content_meta_properties.maintainer}
      />
      {/* the last time the page was reviewed or updated */}
      <meta
        name="embl:last-review"
        content={embl_content_meta_properties.lastreview}
      />
      {/* how long in days before the page should be checked */}
      <meta
        name="embl:review-cycle"
        content={embl_content_meta_properties.reviewcycle}
      />
      {/* if there is a fixed point in time when the page is no longer relevant */}
      <meta name="embl:expiry" content={embl_content_meta_properties.expiry} />

      {/* <!-- analytics --> */}
      <meta name="vf:page-type" content={meta.pagetype} />
    </Helmet>
  );
}
