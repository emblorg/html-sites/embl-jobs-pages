import { memo } from "react";

export const EmblHeader = memo(function EmblHeader() {
  return (
    <link
      rel="import"
      href="https://www.embl.org/api/v1/pattern.html?filter-content-type=article&filter-id=108434&pattern=node-body&source=contenthub"
      data-target="self"
      data-embl-js-content-hub-loader
    />
  );
});
