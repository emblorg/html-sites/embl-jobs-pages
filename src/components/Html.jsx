export function Html({ html }) {
  return (
    <p
      dangerouslySetInnerHTML={{
        __html: html,
      }}
    ></p>
  );
}
