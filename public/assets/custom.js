//Variables

var regex = /langCode=(\w+)/g,
  applyUrl = window.location.href,
  matches = regex.exec(applyUrl);

$(document).ready(function () {
  var lumesseRequire =
    typeof lumesse != "undefined" && "require" in lumesse
      ? lumesse.require
      : require;
  lumesseRequire(
    [
      "ApplyCustomerEvents",
      "jquery-noConflict",
      "underscore-noConflict",
      "backbone-noConflict",
      "ApplicationFormTemplates",
    ],
    function (ApplyCustomerEvents, $, _, Backbone, ApplicationFormTemplates) {
      ApplyCustomerEvents.on(ApplyCustomerEvents.appInitialized, function () {
        if (
          matches[1] === "en" ||
          matches[1] === "en_GB" ||
          matches[1] === "en_US"
        ) {
          ApplicationFormTemplates.set(
            "SubmissionAcceptedComponent",
            '<div class="apply-submission-accepted">\n\t' +
              '<div class="apply-messages">\n\t\t<span class="apply-message-submission-success">' +
              "<p>\n\t\t\t Thank you <%- firstName %> <%- lastName %> for submitting your application to EMBL!\n\t\t</p></span>" +
              '\n\t\t<span class="apply-message-aplication-received">' +
              '<p class="thankYou">We have received your application for  <%- jobName %>.</p>' +
              '<p class="inTouch">Let\'s keep in touch in the meantime</p>' +
              '<ul class="social">' +
              '<li><a href="https://linkedin.com/company/embl" target="_blank">linkedin.com/company/embl</a></li>' +
              '<li><a href="https://twitter.com/embl" target="_blank">twitter.com/embl</a></li>' +
              '<li><a href="https://facebook.com/embl.org" target="_blank">facebook.com/embl.org</a></li>' +
              "</ul>" +
              "</span></div>"
          );
        } else if (matches[1] === "de" || matches[1] === "de_DE") {
          ApplicationFormTemplates.set(
            "SubmissionAcceptedComponent",
            '<div class="apply-submission-accepted">\n\t' +
              '<div class="apply-messages">\n\t\t<span class="apply-message-submission-success">' +
              "<p>\n\t\t\t Vielen Dank <%- firstName %> <%- lastName %> für Ihre Bewerbung am EMBL!\n\t\t</p></span>" +
              '\n\t\t<span class="apply-message-aplication-received">' +
              '<p class="thankYou">Wir haben Ihre Bewerbung für die Stelle  <%- jobName %> erhalten.</p>' +
              '<p class="inTouch">Lassen Sie uns in Kontakt bleiben.</p>' +
              '<ul class="social">' +
              '<li><a href="https://linkedin.com/company/embl" target="_blank">linkedin.com/company/embl</a></li>' +
              '<li><a href="https://twitter.com/embl" target="_blank">twitter.com/embl</a></li>' +
              '<li><a href="https://facebook.com/embl.org" target="_blank">facebook.com/embl.org</a></li>' +
              "</ul>" +
              "</span></div>"
          );
        }
      });
    }
  );
});
