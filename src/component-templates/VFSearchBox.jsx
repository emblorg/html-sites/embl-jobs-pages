import { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { debounce } from "lodash-es";

export function VFSearchBox({ value, handleSearchTermUpdate }) {
  const [term, setTerm] = useState("");
  const handleDebouncedUpdate = useRef(debounce(handleSearchTermUpdate, 500));

  useEffect(() => {
    setTerm(value);
  }, [value]);

  return (
    <form
      data-vf-google-analytics-region="jobs-vacancies-search"
      className="vf-form vf-form--search vf-form--search--responsive | vf-sidebar vf-sidebar--end"
      onSubmit={(e) => {
        e.preventDefault();
      }}
    >
      <div className="vf-sidebar__inner">
        <div className="vf-form__item">
          <label
            className="vf-form__label vf-u-sr-only | vf-search__label"
            htmlFor="textbox-filter"
          >
            Search
          </label>
          <input
            id="textbox-filter"
            type="search"
            value={term}
            onChange={(e) => {
              setTerm(e.target.value);
              handleDebouncedUpdate.current(e.target.value);
            }}
            placeholder="Search jobs"
            className="vf-form__input"
            spellCheck="false"
            style={{ marginTop: "0px" }}
          />
        </div>
        <button className="vf-search__button | vf-button vf-button--primary">
          <span className="vf-button__text">Filter</span>
        </button>
        {/*<a*/}
        {/*  href="#jobFilter"*/}
        {/*  className="vf-search__button | vf-button vf-button--primary"*/}
        {/*>*/}
        {/*  Filter*/}
        {/*</a>*/}
      </div>
    </form>
  );
}

VFSearchBox.propTypes = {
  handleSearchTermUpdate: PropTypes.func,
};
