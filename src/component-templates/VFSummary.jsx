export function VfSummary({
  summary__title,
  summary__text,
  summary__href,
  id,
}) {
  return (
    <article id={id} className="vf-summary">
      <h3 className="vf-summary__title">
        <a href={summary__href} className="vf-summary__link">
          {summary__title}
        </a>
      </h3>
      <p className="vf-summary__text">{summary__text}</p>
    </article>
  );
}
