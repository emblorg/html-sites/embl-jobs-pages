/* ShortcutsList is temporray name, until someone suggests proper name to this component
  @param links // format: [{linkText: "your Role", href: "#role"}]
*/
export function VFShortcutsList({ links }) {
  return (
    <div className="vf-links vf-links__list--easy">
      <ul className="vf-links__list | vf-list">
        {links.map(({ linkText, id }) => (
          <li key={id} className="vf-list__item">
            <a
              className="vf-list__link"
              onClick={() => scrollIntoView(id)}
              style={{ cursor: "pointer" }}
            >
              {linkText}
              <svg
                className="vf-icon vf-icon__arrow--down | vf-list__icon"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
              >
                <title>arrow-button-down</title>
                <path d="M.249,7.207,11.233,19.678h0a1.066,1.066,0,0,0,1.539,0L23.751,7.207a.987.987,0,0,0-.107-1.414l-1.85-1.557a1.028,1.028,0,0,0-1.438.111L12.191,13.8a.25.25,0,0,1-.379,0L3.644,4.346A1.021,1.021,0,0,0,2.948,4a1,1,0,0,0-.741.238L.356,5.793A.988.988,0,0,0,0,6.478.978.978,0,0,0,.249,7.207Z" />
              </svg>
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}

function scrollIntoView(id) {
  document.getElementById(id).scrollIntoView({
    behavior: "smooth",
  });
}
