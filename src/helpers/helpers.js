export const EMAIL_VALIDATION_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

export function stripHtml(inputString = "") {
  return inputString.replace(/(<([^>]+)>|\n)/gi, "");
}

export function toggleInArray(array, element) {
  if (array.includes(element)) {
    return array.filter((el) => el !== element);
  } else {
    array.push(element);
    return array;
  }
}

/**
 *
 * @param  {...any} classList
 * @returns
 */
export const VfClassNormalize = function VfCssClassNormalize(...classList) {
  return classList
    .flat(3) //flattens nested arrays
    .filter((items) => items) //removes falsy items
    .join(" ");
};

/* Parses grade from string
Example inputs: "Grade 9 - 10", "9", "", "Grade 6 or 7"  */
export function parseGrade(input) {
  if (!input) {
    return 0;
  }
  const parsedInt = input.match(/\d+/);
  if (parsedInt) {
    return Number(parsedInt);
  }
  return 0;
}

export function getHostWebsite() {
  var url =
    window.location != window.parent.location
      ? document.referrer
      : document.location.host;
  return url || "";
}
