import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { useFetchJobsList } from "../../services/useFetchJobsList";
import { VFLoaderBox } from "component-templates/VFLoader/VFLoader";
import { JobNotFound } from "components/job-details/JobDetails";
import { useParams } from "react-router";

/* Raison d'être of this component is to Navigate users that come from email alerts to actual job page */
export default function EmailAlertNavigates() {
  const { jobRef } = useParams();
  const { filteredJobs, loading, error } = useFetchJobsList();
  const [job, setJob] = useState();
  useEffect(() => {
    if (filteredJobs && filteredJobs.length) {
      const foundJob = filteredJobs.find((job) => job.id === jobRef);
      setJob(foundJob);
    }
  }, [filteredJobs]);
  if (job) {
    return (
      <Navigate
        to={{
          pathname: `/position/${job.field_jobs_reference_number}`,
          state: { job },
        }}
      />
    );
  } else if (loading) {
    return <VFLoaderBox />;
  } else if (error) {
    return <JobNotFound />;
  }
  return null;
}
