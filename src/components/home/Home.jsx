import { VFSearchBox } from "component-templates/VFSearchBox";
import { JobListFilters } from "components/job-list-filters/JobListFilters";
import { JobList } from "components/job-list/JobList";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useFetchJobsList } from "services/useFetchJobsList";
import { useUrlFilters } from "../../helpers/useUrlFilters";
import "./Home.scss";

export function Home({ showLocations = true, jobLocation, isIframe = false }) {
  const { filters, setFilters, resetFilters } = useUrlFilters();
  const navigate = useNavigate();

  const {
    filteredJobs: jobs,
    locationJobsCount,
    positionJobsCount,
    loading,
    error,
  } = useFetchJobsList(filters, jobLocation);

  // this effect should only run once, when the jobs are loaded
  useEffect(() => {
    if (jobs.length > 0 && filters.nPostingTargetID) {
      const job = jobs.find((j) => j.id === filters.nPostingTargetID);
      if (job) {
        navigate(`/position/${job.field_jobs_reference_number}`, {
          state: { job },
        });
      } else {
        resetFilters();
      }
    }
  }, [jobs]);

  const handleSearch = (searchTerm) => {
    setFilters((filters) => ({
      ...filters,
      searchTerm,
    }));
  };

  return (
    <>
      {error && (
        <div className="vf-box vf-u-background-color--red--light">
          <h3 className="vf-box__heading">Error occurred</h3>
          <p className="vf-box__text">Error details: {error}.</p>
        </div>
      )}
      {!error && (
        <>
          <section className="embl-grid embl-grid--has-centered-content">
            <div></div>
            <VFSearchBox
              handleSearchTermUpdate={handleSearch}
              value={filters.searchTerm}
            />
          </section>
          <section className="embl-grid">
            <JobListFilters
              resetFilters={resetFilters}
              filters={filters}
              locationJobsCount={locationJobsCount}
              positionJobsCount={positionJobsCount}
              onFiltersChange={setFilters}
              loading={loading}
              showLocations={showLocations}
            />

            <JobList
              jobs={jobs}
              loading={loading}
              isIframe={isIframe}
              resetFilters={resetFilters}
            />
          </section>
        </>
      )}
    </>
  );
}
