import { Alerts } from "components/alerts/Alerts";
import { createBrowserHistory } from "history";
import { Apply } from "components/apply/Apply";
import { Partners } from "components/content/Partners";
import { JobDetails } from "components/job-details/JobDetails";
import { Metadata } from "components/Metadata";
import ScrollToAnchor from "components/ScrollToAnchor";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { ErrorPage404 } from "../component-templates/ErrorPages/ErrorPage404";
import EmailAlertRedirects from "../components/alerts/EmailAlertRedirects";
import { HrExcellenceInResearch } from "../components/content/HrExcellenceInResearch";
import { Science4Refugees } from "../components/content/Science4Refugees";
import { EmployeeBenefits } from "../components/content/work-at-embl/EmployeeBenefits";
import { GroupLeaderRecruitment } from "../components/content/work-at-embl/GroupLeaderRecruitment";
import { RecruitmentAtEMBL } from "../components/content/work-at-embl/RecruitmentAtEMBL";
import { InternationalApplicants } from "../components/content/work-at-embl/InternationalApplicants";
import { TraineesInterns } from "../components/content/work-at-embl/TraineesInterns";
import { WorkAtEmbl } from "../components/content/work-at-embl/WorkAtEmbl";
import { EBIHome } from "../components/home/EBIHome";
import { EMBLHome } from "../components/home/EMBLHome";
import "./App.scss";
import { AppContext, isEBISite, orgName } from "./AppContext";
import { Navigate } from "react-router-dom";

var history = createBrowserHistory();

export function App() {
  return (
    <AppContext.Provider value={(isEBISite, orgName)}>
      <Router history={history} basename="jobs">
        <Metadata />
        <ScrollToAnchor />
        <div className="vf-body">
          <Routes>
            <Route path="/" element={<EMBLHome />} />
            <Route path="/position/:jobRef" element={<JobDetails />} />
            <Route path="/apply" element={<Apply />} />
            <Route path="/partners" element={<Partners />} />

            <Route
              path="/alerts/alert-email/:jobRef"
              element={<EmailAlertRedirects />}
            />
            <Route path="/alerts" element={<Alerts />} />

            <Route path="/ebi/iframe" element={<EBIHome />} />
            <Route
              path="/hr-excellence-in-research"
              element={<HrExcellenceInResearch />}
            />
            <Route path="/science4refugees" element={<Science4Refugees />} />
            <Route
              path="/work-at-embl/group-leader-recruitment"
              element={<GroupLeaderRecruitment />}
            />
            <Route
              path="/work-at-embl/benefits"
              element={<EmployeeBenefits />}
            />
            <Route
              path="/work-at-embl/recruitment"
              element={<RecruitmentAtEMBL />}
            />
            <Route
              path="/work-at-embl/international-applicants"
              element={<InternationalApplicants />}
            />
            <Route
              path="/work-at-embl/internships"
              element={<TraineesInterns />}
            />
            <Route exact path="/work-at-embl" element={<WorkAtEmbl />} />
            {/* Redirect urls */}
            <Route
              exact
              path="/work-at-embl/conditions-employment"
              element={<Navigate to="/work-at-embl/benefits" />}
            />
            <Route
              exact
              path="/work-at-embl/conditions-employment/family-life"
              element={<Navigate to="/work-at-embl/benefits" />}
            />
            <Route
              exact
              path="/work-at-embl/conditions-employment/financial-support"
              element={<Navigate to="/work-at-embl/benefits" />}
            />
            <Route
              exact
              path="/work-at-embl/conditions-employment/remuneration"
              element={<Navigate to="/work-at-embl/benefits" />}
            />
            <Route
              exact
              path="/work-at-embl/conditions-employment/social-security"
              element={<Navigate to="/work-at-embl/benefits" />}
            />
            <Route
              exact
              path="/work-at-embl/conditions-employment/training"
              element={<Navigate to="/work-at-embl/benefits" />}
            />
            <Route
              exact
              path="/work-at-embl/conditions-employment/working-hours-leave"
              element={<Navigate to="/work-at-embl/benefits" />}
            />
            <Route
              exact
              path="/work-at-embl/conditions-employment/visa-requirements"
              element={
                <Navigate to="/work-at-embl/international-applicants#visas" />
              }
            />

            {/* default route */}
            <Route path="*" element={<ErrorPage404 />} />
          </Routes>
        </div>
      </Router>
    </AppContext.Provider>
  );
}
