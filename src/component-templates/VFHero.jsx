import React from "react";

export default function VFHero({
  spacing = 800,
  heading = "",
  subheading = "",
  link_text = "",
  link_href = "",
  image_url = "",
  image_size = "auto 28.5rem",
}) {
  return (
    <section
      className={`vf-hero vf-hero--${spacing} | vf-u-fullbleed`}
      style={{
        "--vf-hero--bg-image": `url(${image_url})`,
        "--vf-hero--bg-image-size": image_size,
      }}
    >
      <div className="vf-hero__content | vf-box | vf-stack vf-stack--400">
        <h1 className="vf-hero__heading">{heading}</h1>
        <p className="vf-hero__subheading">{subheading}</p>
        <a className="vf-hero__link" href={link_href}>
          {link_text}
        </a>
      </div>
    </section>
  );
}
