import { ContentMeta } from "../components/ContentMeta";
import { EMBLContentTemplate } from "../components/EMBLContentTemplate";

export function WorkAtEmbl() {
  return (
    <EMBLContentTemplate>
      <ContentMeta
        keywords="basic research, molecular biology, structural biology, cell biology, biophysics, developmental biology, chemical biology, bioinformatics, PhD Programme, technology transfer, Postdoctoral Programme, epigenetics, neurobiology, tissue Biology, disease modelling, computational biology, work at embl, jobs at embl"
        description="Join EMBL and you’ll join a community of world-class researchers and skilled professionals working together to uncover the secrets of life."
        canonical="https://www.embl.org/jobs/work-at-embl/"
      />

      {/* Breadcrumbs */}
      <nav className="vf-breadcrumbs" aria-label="Breadcrumb">
        <ul className="vf-breadcrumbs__list | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/jobs/"
              className="vf-breadcrumbs__link"
            >
              Jobs
            </a>
          </li>
          <li className="vf-breadcrumbs__item" aria-current="location">
            Careers at EMBL
          </li>
        </ul>

        <span className="vf-breadcrumbs__heading">Related:</span>
        <ul className="vf-breadcrumbs__list vf-breadcrumbs__list--related | vf-list vf-list--inline">
          <li className="vf-breadcrumbs__item">
            <a
              href="https://www.embl.org/sites"
              className="vf-breadcrumbs__link"
            >
              All EMBL sites
            </a>
          </li>
        </ul>
      </nav>

      {/* Content */}
      <div className="vf-content">
        <link
          rel="import"
          href="https://www.embl.org/api/v1/pattern.html?filter-content-type=article&filter-id=180957&pattern=node-body&source=contenthub"
          data-target="self"
          data-embl-js-content-hub-loader
        />
      </div>
    </EMBLContentTemplate>
  );
}
