import { createContext } from "react";
import { getHostWebsite } from "../helpers/helpers";

export const isEBISite = getHostWebsite().includes("ebi.ac.uk");
export const orgName = isEBISite ? "EBI" : "EMBL-EBI";
export const AppContext = createContext({ isEBISite, orgName });
