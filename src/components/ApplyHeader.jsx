import { memo } from "react";
import { useEffect } from "react";

// The jobs client side javascript is very particular about load order
export const ApplyHeader = memo(function ApplyHeader() {
  useEffect(() => {
    const jquery = document.createElement("script");
    jquery.async = false;
    jquery.src = "https://code.jquery.com/jquery-1.12.4.min.js";
    document.body.appendChild(jquery);

    const bootstrap = document.createElement("script");
    bootstrap.async = false;
    bootstrap.src =
      "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js";
    document.body.appendChild(bootstrap);

    const jobsPreloader = document.createElement("script");
    jobsPreloader.dataset.lumesseApply = true;
    jobsPreloader.dataset.lumesseApplyConfigKey =
      "AAAC8QAA-1d049852-a5bd-42da-bd30-ebe6eec9b143";
    jobsPreloader.dataset.lumesseApplyHost =
      "https://emea3.recruitmentplatform.com";
    jobsPreloader.dataset.lumesseApplyErrorsPlacement = "bottom";
    jobsPreloader.dataset.lumesseApplyDescriptionPlacement = "bottom";
    jobsPreloader.dataset.lumesseApplyMenuPlacement = "left";
    jobsPreloader.async = false;
    jobsPreloader.src =
      "https://emea3.recruitmentplatform.com/apply-app/static/apply/release/2-LATEST/apply-preloader-namespaced.js";
    document.body.appendChild(jobsPreloader);

    const jobsCustom = document.createElement("script");
    jobsCustom.async = false;
    jobsCustom.src = "/jobs/assets/custom.js";
    document.body.appendChild(jobsCustom);

    const jobsMain = document.createElement("script");
    jobsMain.async = false;
    jobsMain.src = "/jobs/assets/main.js";
    document.body.appendChild(jobsMain);

    return () => {
      document.body.removeChild(jquery);
      document.body.removeChild(bootstrap);
      document.body.removeChild(jobsPreloader);
      document.body.removeChild(jobsCustom);
      document.body.removeChild(jobsMain);
    };
  }, []);
  return <></>;
});
