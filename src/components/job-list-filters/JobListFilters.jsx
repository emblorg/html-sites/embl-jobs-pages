import { VFLoaderBox } from "component-templates/VFLoader/VFLoader";
import { toggleInArray } from "helpers/helpers";
import "./JobListFilters.scss";

export function JobListFilters({
  filters,
  locationJobsCount = [],
  positionJobsCount = [],
  onFiltersChange,
  loading,
  showLocations = true,
  resetFilters,
}) {
  const handleLocationFilterChange = (location) => {
    // toggle this element in array
    let selectedLocations = toggleInArray(filters.selectedLocations, location);

    onFiltersChange((filters) => ({
      ...filters,
      selectedLocations,
    }));
  };

  const handleJobTypeFilterChange = (targetJobTypeKey) => {
    // toggle this element in array
    let selectedJobTypes = toggleInArray(
      filters.selectedJobTypes,
      targetJobTypeKey
    );

    onFiltersChange((filters) => ({
      ...filters,
      selectedJobTypes,
    }));
  };

  const handleClosingDateSortDirectionChange = (selectedClosingDateSortDir) => {
    onFiltersChange((filters) => ({
      ...filters,
      selectedClosingDateSortDir,
    }));
  };

  const handleResetFilters = (event) => {
    event.preventDefault();
    resetFilters();
  };

  if (loading) {
    return <VFLoaderBox />;
  }

  return (
    <form
      className="vf-form filters-form | vf-stack vf-stack--800"
      data-vf-google-analytics-region="jobs-vacancies-filters"
    >
      {showLocations && (
        <fieldset className="vf-form__fieldset | vf-stack vf-stack--400">
          <legend className="vf-form__legend">Position location</legend>
          {locationJobsCount.map(
            ({ location, locationTitle, jobCount }, index) => (
              <div
                key={location}
                className="vf-form__item vf-form__item--checkbox"
              >
                <input
                  type="checkbox"
                  id={`location-checkbox-${index}`}
                  className="vf-form__checkbox"
                  checked={filters.selectedLocations.includes(location)}
                  onChange={() => handleLocationFilterChange(location)}
                />
                <label
                  htmlFor={`location-checkbox-${index}`}
                  className="vf-form__label"
                >
                  {locationTitle}
                  {` `}({jobCount})
                </label>
              </div>
            )
          )}
        </fieldset>
      )}
      <fieldset className="vf-form__fieldset | vf-stack vf-stack--400">
        <legend className="vf-form__legend">Type</legend>
        {positionJobsCount.map(
          ({ position, positionTitle, jobCount }, index) => (
            <div
              key={position}
              className="vf-form__item vf-form__item--checkbox"
            >
              <input
                type="checkbox"
                id={`jobtype-checkbox-${index}`}
                className="vf-form__checkbox"
                checked={filters.selectedJobTypes.includes(position)}
                onChange={() => handleJobTypeFilterChange(position)}
              />
              <label
                htmlFor={`jobtype-checkbox-${index}`}
                className="vf-form__label"
              >
                {positionTitle} ({jobCount})
              </label>
            </div>
          )
        )}
      </fieldset>
      <fieldset className="vf-form__fieldset | vf-stack vf-stack--400">
        <legend className="vf-form__legend">Sort by time</legend>
        <div className="vf-form__item vf-form__item--radio">
          <input
            type="radio"
            className="vf-form__radio"
            id="sort-id1"
            name="sort1"
            onChange={() => handleClosingDateSortDirectionChange("ASC")}
            checked={filters.selectedClosingDateSortDir === "ASC"}
          />
          <label htmlFor="sort-id1" className="vf-form__label">
            Closing first
          </label>
        </div>
        <div className="vf-form__item vf-form__item--radio">
          <input
            type="radio"
            className="vf-form__radio"
            id="sort-id2"
            name="sort1"
            onChange={() => handleClosingDateSortDirectionChange("DESC")}
            checked={filters.selectedClosingDateSortDir === "DESC"}
          />
          <label htmlFor="sort-id2" className="vf-form__label">
            Closing last
          </label>
        </div>
      </fieldset>
      <button
        className="vf-button vf-button--sm vf-button--primary"
        onClick={handleResetFilters}
      >
        Reset filters
      </button>
    </form>
  );
}
